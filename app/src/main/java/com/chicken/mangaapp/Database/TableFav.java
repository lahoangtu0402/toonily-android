package com.chicken.mangaapp.Database;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import com.chicken.mangaapp.Model.Toonily;

import java.util.ArrayList;

public class TableFav implements BaseColumns {
    public static final String TABLE_NAME = "table_fav";
    public static final String NAME = "name_comic";
    public static final String SLUG = "slug_comic";
    public static final String THUMB = "thumb_comic";
    public static final String CREATE_AT = "createat";

    //


    public static DBHelper dbHelper;
    private SQLiteDatabase db;
    public TableFav(Activity activity) {
        dbHelper = new DBHelper(activity);
        db = dbHelper.getWritableDatabase();
    }

    // lay ra all
    public ArrayList<Toonily> getListFav(){
        //
        ArrayList<Toonily> list = new ArrayList<Toonily>();

        // Select All Query
        String sql = "SELECT * FROM "+ TableFav.TABLE_NAME + " ORDER BY " + TableFav.CREATE_AT + " DESC  ;";

        Cursor cursor =  dbHelper.GetData(sql);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String slug = cursor.getString(cursor.getColumnIndex(TableFav.SLUG));
                String name = cursor.getString(cursor.getColumnIndex(TableFav.NAME));
                String thumb = cursor.getString(cursor.getColumnIndex(TableFav.THUMB));
                //
                Toonily note = new Toonily(name,slug,thumb);
                list.add(note);
            } while (cursor.moveToNext());
        }

        return list;
    }

    // them
    public boolean addToFav(Toonily info){

        ContentValues contentValues = new ContentValues();
        contentValues.put(TableFav.NAME,info.getComicName());
        contentValues.put(TableFav.SLUG,info.getComicSlug());
        contentValues.put(TableFav.THUMB,info.getComicThumb());

        long result = 0;
        try {
            db = dbHelper.getWritableDatabase();
            result = db.insert(TableFav.TABLE_NAME, null, contentValues);
        }catch(Exception ex) {
            Log.e("Error DB", ex.getMessage());
        }
        return result > 0;



    }

    // xoa
    public boolean deleteFav(String slug){
        int i = db.delete(TableFav.TABLE_NAME,//ten bang
                TableFav.SLUG+" = ?",//dieu kien
                new String[] { slug });
        if(i>0){return true;}
        return false;
    }

    //check exist
    public boolean checkExist(String slug){
        String sql = "SELECT * FROM "+ TableFav.TABLE_NAME + " WHERE "+ TableFav.SLUG + " = '"+slug+"'";
        Cursor cursor =  dbHelper.GetData(sql);
        if (cursor.getCount() == 0 ){
            return false;
        }
        return  true;
    }

}
