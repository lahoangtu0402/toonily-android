package com.chicken.mangaapp.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "toonily.db";
    private static final int DATABASE_VERSION = 1;
    private static Context c;

    //single stance
    private static DBHelper instance;
    public static DBHelper getInstance(Context context) {
        if (instance == null) {
            synchronized (DBHelper.class) {
                instance = new DBHelper(context, "toonily.sqlite", null, 1);
            }
        }
        return instance;
    }

//    //New DB
    final String SQL_CREATE_TABLE_FAV =
        "CREATE TABLE " + TableFav.TABLE_NAME + " (" +
                TableFav._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                TableFav.NAME + " TEXT,"+
                TableFav.THUMB + " TEXT,"+
                TableFav.SLUG + "  TEXT,"+
                TableFav.CREATE_AT + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP " + ")";



    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void QueryData(String sql)
    {
        try {
            SQLiteDatabase database = getWritableDatabase();
            database.execSQL(sql);
        }catch (Exception e){

        }
    }

    public Cursor GetData(String sql)
    {
        try {
            SQLiteDatabase database = getReadableDatabase();
            return database.rawQuery(sql,null);
        }catch (Exception e){
            return null;
        }
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

//        //For New User
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_FAV);
//

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        //For User Update
        if(newVersion>oldVersion){


        }else {

        }
    }
}
