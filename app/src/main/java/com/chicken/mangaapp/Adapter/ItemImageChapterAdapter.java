package com.chicken.mangaapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chicken.mangaapp.R;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ItemImageChapterAdapter extends RecyclerView.Adapter<ItemImageChapterAdapter.ViewHoder> {
    ArrayList<String> arrayList;
    Context context;
    Picasso picasso;

    public ItemImageChapterAdapter(Context context, ArrayList<String> arrayList ,String referer ) {
        this.arrayList = arrayList;
        this.context = context;
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Interceptor.Chain chain) throws IOException {
                        Request newRequest = chain.request().newBuilder()
                                .addHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36")
                                .addHeader("Referer", referer)
                                .build();
                        return chain.proceed(newRequest);
                    }
                })
                .build();
        picasso = new Picasso.Builder(context)
                .downloader(new OkHttp3Downloader(client))
                .build();

    }


    @NonNull
    @Override
    public ItemImageChapterAdapter.ViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_image_chapter, parent, false);
        return new ItemImageChapterAdapter.ViewHoder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ItemImageChapterAdapter.ViewHoder holder, int position) {

        holder.image_chapter.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                picasso.load(arrayList.get(position)).placeholder(R.drawable.placeholder)
                        .error(R.drawable.ic_baseline_home_24)
                        .resize(holder.image_chapter.getWidth(), 0)
                        .centerInside()
                        .into(holder.image_chapter);
                holder.image_chapter.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

       // Log.d("image_chapter",arrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    class ViewHoder extends RecyclerView.ViewHolder{
        ImageView image_chapter;
        public ViewHoder(@NonNull View itemView) {
            super(itemView);
            image_chapter = itemView.findViewById(R.id.image_chapter);

        }
    }
}
