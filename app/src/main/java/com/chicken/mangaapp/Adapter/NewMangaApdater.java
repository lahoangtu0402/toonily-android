package com.chicken.mangaapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chicken.mangaapp.ItemClick;
import com.chicken.mangaapp.Model.Toonily;
import com.chicken.mangaapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class NewMangaApdater extends RecyclerView.Adapter<NewMangaApdater.ViewHoder> {
    ArrayList<Toonily> arrayList;
    Context context;
    ItemClick itemClick;

    public void setItemClick(ItemClick itemClick) {
        this.itemClick = itemClick;
    }

    public NewMangaApdater(Context context, ArrayList<Toonily> arrayList ) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public NewMangaApdater.ViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_new_manga, parent, false);
        return new NewMangaApdater.ViewHoder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewMangaApdater.ViewHoder holder, int position) {
        holder.txt_title.setText(arrayList.get(position).getComicName());
        Picasso.get().load(arrayList.get(position).getComicThumb()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    class ViewHoder extends RecyclerView.ViewHolder{
        TextView txt_title;
        ImageView image;
        public ViewHoder(@NonNull View itemView) {
            super(itemView);
            txt_title = itemView.findViewById(R.id.txt_title);
            image = itemView.findViewById(R.id.image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClick.onClickItem(getAdapterPosition());
                }
            });
        }
    }
}
