package com.chicken.mangaapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chicken.mangaapp.ItemClick;
import com.chicken.mangaapp.Model.Toonily;
import com.chicken.mangaapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TopTrendingAdapter extends RecyclerView.Adapter<TopTrendingAdapter.ViewHoder> {
    ArrayList<Toonily> arrayList;
    Context context;
    ItemClick itemClick;

    public void setItemClick(ItemClick itemClick) {
        this.itemClick = itemClick;
    }

    public TopTrendingAdapter(Context context, ArrayList<Toonily> arrayList ) {
        this.arrayList = arrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_top_trending, parent, false);
        return new ViewHoder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHoder holder, int position) {
        holder.txt_stt.setText("#"+(position+1));
        holder.txt_title.setText(arrayList.get(position).getComicName());
        holder.txtDate.setText("Create at: "+arrayList.get(position).getCreatedAt());
        Picasso.get().load(arrayList.get(position).getComicThumb()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    class ViewHoder extends RecyclerView.ViewHolder{
        TextView txt_stt,txt_title,txtDate;
        ImageView image;
        public ViewHoder(@NonNull View itemView) {
            super(itemView);
            txt_stt = itemView.findViewById(R.id.txt_stt);
            txt_title = itemView.findViewById(R.id.txt_title);
            txtDate = itemView.findViewById(R.id.txtDate);
            image = itemView.findViewById(R.id.image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClick.onClickItem(getAdapterPosition());
                }
            });
        }
    }
}
