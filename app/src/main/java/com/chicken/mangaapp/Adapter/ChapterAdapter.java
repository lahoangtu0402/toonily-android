package com.chicken.mangaapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chicken.mangaapp.ItemClick;
import com.chicken.mangaapp.Model.Chapter;
import com.chicken.mangaapp.R;

import java.util.ArrayList;

public class ChapterAdapter extends RecyclerView.Adapter<ChapterAdapter.ViewHoder> {
    ArrayList<Chapter> arrayList;
    Context context;
    ItemClick itemClick;
    boolean isSearch = false;

    public void setItemClick(ItemClick itemClick) {
        this.itemClick = itemClick;
    }

    public ChapterAdapter(Context context, ArrayList<Chapter> arrayList ) {
        this.arrayList = arrayList;
        this.context = context;
    }


    @NonNull
    @Override
    public ChapterAdapter.ViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_chapter, parent, false);
        return new ChapterAdapter.ViewHoder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ChapterAdapter.ViewHoder holder, int position) {
        Chapter chapter = arrayList.get(position);
        holder.txt_title.setText(chapter.getChapterName());
        holder.txtDate.setText(chapter.getCreatedAt());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    class ViewHoder extends RecyclerView.ViewHolder{
        TextView txt_title,txtDate;
        public ViewHoder(@NonNull View itemView) {
            super(itemView);
            txt_title = itemView.findViewById(R.id.txt_title);
            txtDate = itemView.findViewById(R.id.txtDate);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClick.onClickItem(getAdapterPosition());
                }
            });
        }
    }
}
